import axios from "axios";

const axiosConfig = {
  baseURL: process.env.REACT_APP_BACKEND_HOST || "http://localhost:4000/api",
  headers: {
    "Content-Type": "application/json",
  },
};

export default axios.create(axiosConfig);
