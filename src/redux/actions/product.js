import axios from "redux/actions/axios";
import { GET_PRODUCT, CLEAR_PRODUCT } from "redux/actions/action-types";

import Product from "domain-objects/product";

export function fetchProduct(id) {
  return (dispatch, getState) => {
    axios.get(`/products/${id}/`).then((response) =>
      dispatch({
        type: GET_PRODUCT,
        payload: new Product(response.data.data),
      })
    );
  };
}

export function clearProduct() {
  return (dispatch, getState) => dispatch({ type: CLEAR_PRODUCT });
}
