import {
  ADD_ONE_PRODUCT_TO_TROLLEY,
  REMOVE_ONE_PRODUCT_FROM_TROLLEY,
  DELETE_PRODUCT_FROM_TROLLEY,
  SET_TROLLEY,
  CLEAR_TROLLEY,
} from "redux/actions/action-types";

import Trolley from "domain-objects/trolley";

export function addOneItem(product) {
  return (dispatch, getState) => {
    dispatch({ type: ADD_ONE_PRODUCT_TO_TROLLEY, payload: product });
  };
}

export function removeOneItem(product) {
  return (dispatch, getState) => {
    dispatch({ type: REMOVE_ONE_PRODUCT_FROM_TROLLEY, payload: product });
  };
}

export function deleteItem(product) {
  return (dispatch, getState) => {
    dispatch({ type: DELETE_PRODUCT_FROM_TROLLEY, payload: product });
  };
}

export function setTrolley(trolley) {
  return (dispatch, getState) => {
    dispatch({ type: SET_TROLLEY, payload: new Trolley(trolley) });
  };
}

export function clearTrolley() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_TROLLEY });
  };
}
