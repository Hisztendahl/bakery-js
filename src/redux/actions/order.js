import axios from "redux/actions/axios";

import {
  PLACE_ORDER,
  FETCH_ORDER,
  CLEAR_ORDER,
  SET_TROLLEY,
  SET_DELIVERY,
  CONFIRM_ORDER,
} from "redux/actions/action-types";
import { createOrder } from "domain-objects/order";
import { createTrolley } from "domain-objects/trolley";
import Delivery from "domain-objects/delivery";

export function placeOrder() {
  return (dispatch, getState) => {
    const order = createOrder({
      delivery: getState().delivery,
      trolley: getState().trolley,
    });
    axios.post("/orders/", order.renderJSON()).then((response) => {
      dispatch({
        type: PLACE_ORDER,
        payload: createOrder(response.data.data),
      });
    });
  };
}

export function fetchOrder(orderId, confirmationKey) {
  return (dispatch, getState) => {
    axios.get(`/orders/${orderId}/${confirmationKey}`).then((response) => {
      dispatch({
        type: FETCH_ORDER,
        payload: createOrder(response.data.data),
      });
      dispatch({
        type: SET_TROLLEY,
        payload: createTrolley(response.data.data.trolley),
      });
      dispatch({
        type: SET_DELIVERY,
        payload: new Delivery(response.data.data.delivery),
      });
    });
  };
}

export function clearOrder() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_ORDER });
  };
}

export function confirmOrder(orderId, confirmationKey) {
  return (dispatch, getState) => {
    axios.put(`/orders/${orderId}/${confirmationKey}`).then((response) => {
      dispatch({
        type: CONFIRM_ORDER,
        payload: createOrder(response.data.data),
      });
    });
  };
}
