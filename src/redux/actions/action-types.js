export const ADD_ONE_PRODUCT_TO_TROLLEY = "ADD_ONE_PRODUCT_TO_TROLLEY";
export const REMOVE_ONE_PRODUCT_FROM_TROLLEY =
  "REMOVE_ONE_PRODUCT_FROM_TROLLEY";
export const DELETE_PRODUCT_FROM_TROLLEY = "DELETE_PRODUCT_FROM_TROLLEY";
export const SET_TROLLEY = "SET_TROLLEY";
export const CLEAR_TROLLEY = "CLEAR_TROLLEY";

export const ADD_DELIVERY = "ADD_DELIVERY";
export const SET_DELIVERY = "SET_DELIVERY";

export const ADD_MESSAGE = "ADD_MESSAGE";
export const POP_MESSAGE = "POP_MESSAGE";

export const GET_PRODUCTS = "GET_PRODUCTS";
export const CLEAR_PRODUCTS = "CLEAR_PRODUCTS";
export const GET_PRODUCT = "GET_PRODUCT";
export const CLEAR_PRODUCT = "CLEAR_PRODUCT";

export const PLACE_ORDER = "PLACE_ORDER";
export const CLEAR_ORDER = "CLEAR_ORDER";
export const FETCH_ORDER = "FETCH_ORDER";
export const CONFIRM_ORDER = "CONFIRM_ORDER";

export const GET_RECIPE = "GET_RECIPE";
export const CLEAR_RECIPE = "CLEAR_RECIPE";

export const SET_PAYMENT = "SET_PAYMENT";
export const CLEAR_PAYMENT = "CLEAR_PAYMENT";

export const CLEAR_FORM_ERROR_MANAGER = "CLEAR_FORM_ERROR_MANAGER";
export const SET_FORM_ERROR_MANAGER = "SET_FORM_ERROR_MANAGER";
