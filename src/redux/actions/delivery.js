import { ADD_DELIVERY } from "redux/actions/action-types";

export function addDelivery(delivery, callback) {
  return (dispatch, getState) => {
    dispatch({ type: ADD_DELIVERY, payload: delivery });
    if (callback) callback(getState().delivery);
  };
}
