import { ADD_DELIVERY, SET_DELIVERY } from "redux/actions/action-types";

import Delivery from "domain-objects/delivery";

export function deliveryReducer(state = new Delivery(), action) {
  switch (action.type) {
    case ADD_DELIVERY:
    case SET_DELIVERY: {
      return action.payload;
    }
    default: {
      return state;
    }
  }
}
