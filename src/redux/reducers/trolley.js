import {
  ADD_ONE_PRODUCT_TO_TROLLEY,
  REMOVE_ONE_PRODUCT_FROM_TROLLEY,
  DELETE_PRODUCT_FROM_TROLLEY,
  SET_TROLLEY,
  CLEAR_TROLLEY,
} from "redux/actions/action-types";

import Trolley from "domain-objects/trolley";

export function trolleyReducer(state = new Trolley(), action) {
  switch (action.type) {
    case ADD_ONE_PRODUCT_TO_TROLLEY: {
      return state.addOneItem(action.payload);
    }
    case REMOVE_ONE_PRODUCT_FROM_TROLLEY: {
      return state.removeOneItem(action.payload);
    }
    case DELETE_PRODUCT_FROM_TROLLEY: {
      return state.deleteItem(action.payload);
    }
    case SET_TROLLEY: {
      return action.payload;
    }
    case CLEAR_TROLLEY: {
      return new Trolley();
    }
    default: {
      return state;
    }
  }
}
