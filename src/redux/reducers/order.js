import {
  PLACE_ORDER,
  CLEAR_ORDER,
  FETCH_ORDER,
} from "redux/actions/action-types";

import { createOrder } from "domain-objects/order";

export function orderReducer(state = createOrder(), action) {
  switch (action.type) {
    case FETCH_ORDER:
    case PLACE_ORDER: {
      return action.payload;
    }
    case CLEAR_ORDER: {
      return createOrder();
    }
    default: {
      return state;
    }
  }
}
