import { combineReducers, applyMiddleware, createStore, compose } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

import { trolleyReducer } from "redux/reducers/trolley";
import { deliveryReducer } from "redux/reducers/delivery";
import { productManagerReducer } from "redux/reducers/product-manager";
import { productReducer } from "redux/reducers/product";
import { recipeReducer } from "redux/reducers/recipe";
import { orderReducer } from "redux/reducers/order";
import { paymentReducer } from "redux/reducers/payment";
import { formErrorManagerReducer } from "redux/reducers/form-error-manager";

const logger = createLogger({ collapsed: true });

const createStoreWithMiddleware = applyMiddleware(thunk, logger)(createStore);

const enhancers = compose(
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
);

const rootReducer = combineReducers({
  trolley: trolleyReducer,
  delivery: deliveryReducer,
  productManager: productManagerReducer,
  product: productReducer,
  recipe: recipeReducer,
  order: orderReducer,
  payment: paymentReducer,
  formErrorManager: formErrorManagerReducer,
});

export default createStoreWithMiddleware(rootReducer, enhancers);
