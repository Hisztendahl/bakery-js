import Product from "domain-objects/product";

export default class ProductManager {
  constructor(items = []) {
    this.items = items;
  }

  getProduct(id) {
    return this.items.find((item) => item.id === id) || new Product();
  }

  filter(type = "all") {
    if (type === "all") return this.data;
    else return this.items.filter((product) => product.type === type);
  }
}
