export default class Product {
  constructor({
    id,
    key = "",
    name = "",
    description = "",
    image = "",
    price = 0,
    type = "bread",
  } = {}) {
    this.id = id;
    this.key = key;
    this.name = name;
    this.description = description;
    this.image = image;
    this.price = price;
    this.type = type;
  }
}
