export default class TrolleyItem {
  constructor({ id, product_id, quantity = 0, unit_price = 0 }) {
    this.id = id;
    this.product_id = product_id;
    this.quantity = quantity;
    this.unit_price = unit_price;
  }

  calculateTotal() {
    return Math.round(this.quantity * this.unit_price * 100) / 100;
  }
}
