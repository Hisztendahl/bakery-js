import Delivery from "domain-objects/delivery";
import Trolley from "domain-objects/trolley";
import TrolleyItem from "domain-objects/trolley-item";

export default class Order {
  constructor({ id, delivery = new Delivery(), trolley = new Trolley() } = {}) {
    this.id = id;
    this.delivery = delivery;
    this.trolley = trolley;
  }

  renderJSON() {
    return {
      order: {
        id: this.id,
        delivery: this.delivery,
        trolley: this.trolley,
      },
    };
  }
}

export function createOrder(params = new Order()) {
  return new Order({
    ...params,
    delivery: new Delivery({
      ...params.delivery,
      delivery_date: new Date(params.delivery.delivery_date),
    }),
    trolley: new Trolley({
      ...params.trolley,
      items: params.trolley.items.map((item) => new TrolleyItem(item)),
    }),
  });
}
