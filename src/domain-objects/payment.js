export default class Payment {
  constructor({ type = "" } = {}) {
    this.type = type;
  }

  isValid() {
    return this.type === "cash";
  }
}
