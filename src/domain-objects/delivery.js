import { formatDate, nextOpenDay } from "utils/date";

export default class Delivery {
  constructor({
    delivery_date = nextOpenDay(),
    first_name = "",
    last_name = "",
    email = "",
    phone = "",
    address1 = "",
    address2 = "",
    city = "Dublin",
    county = "Dublin",
    message = "",
  } = {}) {
    this.delivery_date = delivery_date;
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phone = phone;
    this.address1 = address1;
    this.address2 = address2;
    this.city = city;
    this.county = county;
    this.message = message;
  }

  handleFieldChange(field, value) {
    return new Delivery({ ...this, [field]: value });
  }

  isFieldValid(field) {
    switch (field) {
      case "delivery_date": {
        return this[field] !== null;
      }
      default: {
        return this[field] !== "";
      }
    }
  }

  isValid() {
    return (
      this.delivery_date !== null &&
      this.first_name !== "" &&
      this.last_name !== "" &&
      this.email !== "" &&
      this.phone !== "" &&
      this.address1 !== "" &&
      this.address2 !== "" &&
      this.city !== "" &&
      this.county !== ""
    );
  }

  renderField(field) {
    switch (field) {
      case "delivery_date": {
        return formatDate(this[field]);
      }
      default: {
        return this[field];
      }
    }
  }
}
