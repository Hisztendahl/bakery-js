export default class FormError {
  constructor({ field, value = [] } = {}) {
    this.field = field;
    this.value = value;
  }
}
