import React, { createRef } from "react";
import { Icon, Ref, Sticky, Container, Menu } from "semantic-ui-react";
import { Link, useLocation } from "react-router-dom";

import AppHeader from "components/app-header";
import TrolleyButton from "components/trolley/trolley-button";
import InstagramLink from "components/instagram-link";

const AppLayout = ({ children }) => {
  const { pathname } = useLocation();
  const contextRef = createRef();

  return (
    <Ref innerRef={contextRef}>
      <Container>
        <AppHeader />
        <InstagramLink />
        <Sticky context={contextRef}>
          <Menu stackable>
            <Menu.Item
              color="orange"
              active={pathname === "/" || pathname.startsWith("/products")}
              as={Link}
              link
              to="/"
            >
              <Icon name="food" />
              Products
            </Menu.Item>
            <Menu.Item
              color="blue"
              active={pathname === "/about"}
              as={Link}
              to="/about"
            >
              <Icon name="info" />
              About
            </Menu.Item>
            <Menu.Item
              color="teal"
              active={pathname === "/contact"}
              as={Link}
              to="/contact"
            >
              <Icon name="mail" />
              Contact
            </Menu.Item>
            <Menu.Item
              position="right"
              active={pathname === "/view-trolley"}
              as={Link}
              to="/view-trolley"
              color="green"
            >
              <TrolleyButton />
            </Menu.Item>
          </Menu>
        </Sticky>
        {children}
      </Container>
    </Ref>
  );
};

export default AppLayout;
