import moment from "moment";

const DATE_FORMATS = {
  frontend_datetime: "DD/MM/YYYY HH:mm",
  frontend_date: "DD/MM/YYYY",
  frontend_time: "HH:mm",
  backend_datetime: "YYYY-MM-DD HH:mm:00",
  backend_date: "YYYY-MM-DD",
  backend_time: "HH:mm:00",
};

export function formatDateTime(datetime) {
  if (!datetime) {
    return "";
  }
  return moment
    .utc(datetime, moment.ISO_8601)
    .local()
    .format(DATE_FORMATS.frontend_datetime);
}

export function formatDate(date) {
  if (!date) {
    return "";
  }
  return moment
    .utc(date, moment.ISO_8601)
    .local()
    .format(DATE_FORMATS.frontend_date);
}

export function nextOpenDay() {
  // sunday: 0
  // monday: 1
  // tuesday: 2
  // wednesday: 3
  // thursday: 4
  // friday: 5
  // saturday: 6
  // sunday: 7
  const today = moment().local();
  const dayAfterTomorrow = today.add(2, "days");
  let nextOpenDay = dayAfterTomorrow;
  if (dayAfterTomorrow.weekday() === 1) {
    nextOpenDay = dayAfterTomorrow.add(2, "days");
  }
  if (dayAfterTomorrow.weekday() === 2) {
    nextOpenDay = dayAfterTomorrow.add(1, "days");
  }
  return nextOpenDay.toDate();
}
