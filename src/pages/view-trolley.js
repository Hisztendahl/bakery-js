import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Grid, Segment, Button, Message } from "semantic-ui-react";
import { Link } from "react-router-dom";

import StepBar from "components/step-bar";
import FirstColumn from "components/view-trolley/first-column";
import MiddleColumn from "components/view-trolley/middle-column";
import LastColumn from "components/view-trolley/last-column";
import Delivery from "components/view-trolley/delivery";
import Total from "components/view-trolley/total";

import { fetchProducts, clearProducts } from "redux/actions/product-manager";

const ViewTrolleyPage = ({
  removeOneItem,
  trolley,
  productManager,
  fetchProducts,
  clearProducts,
}) => {
  useEffect(() => {
    fetchProducts();
    return () => clearProducts();
  }, [fetchProducts, clearProducts]);

  return (
    <div>
      <StepBar step="view-trolley" />
      {trolley.calculateTotalWithoutDelivery() < 10 && (
        <Message warning>
          You need to spend €10 or more before delivery to make an order.
        </Message>
      )}
      <Segment basic>
        {trolley.numberItems() > 0 ? (
          <>
            <Grid divided="vertically">
              {productManager.items.length > 0 &&
                trolley.items.map((trolleyItem) => {
                  const product = productManager.getProduct(
                    trolleyItem.product_id
                  );
                  return (
                    <Grid.Row key={product.key}>
                      <Grid.Column width={4}>
                        <FirstColumn product={product} />
                      </Grid.Column>
                      <Grid.Column width={9}>
                        <MiddleColumn
                          product={product}
                          trolleyItem={trolleyItem}
                        />
                      </Grid.Column>
                      <Grid.Column
                        width={3}
                        textAlign="right"
                        verticalAlign="middle"
                      >
                        <LastColumn trolleyItem={trolleyItem} />
                      </Grid.Column>
                    </Grid.Row>
                  );
                })}
              <Grid.Row columns={3}>
                <Delivery trolley={trolley} />
              </Grid.Row>
              <Grid.Row columns={3}>
                <Total trolley={trolley} />
              </Grid.Row>
            </Grid>
            <div>
              <Link to="/delivery">
                <Button
                  floated="right"
                  primary
                  disabled={
                    trolley.numberItems() === 0 ||
                    trolley.calculateTotal() -
                      trolley.calculateDeliveryPrice() <
                      10
                  }
                >
                  Continue to delivery
                </Button>
              </Link>
            </div>
          </>
        ) : (
          <p>Trolley is empty</p>
        )}
      </Segment>
    </div>
  );
};

const mapStateToProps = ({ trolley, productManager }) => ({
  trolley,
  productManager,
});
const mapDispatchToProps = {
  fetchProducts,
  clearProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewTrolleyPage);
