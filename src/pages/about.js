import React from "react";
import { Segment } from "semantic-ui-react";

const GenericNotFound = () => {
  return (
    <Segment basic>
      <h1>About us</h1>
      <p>
        Francois' Bakery is located at{" "}
        <a href="https://www.instagram.com/elmhurstcottagefarm/">
          Elmhurst Cottage Farm
        </a>
        , in Glasnevin, Dublin.
      </p>
      <p>
        We propose a range of French bread and pastries. All the recipes are
        available in the shop
      </p>
    </Segment>
  );
};

export default GenericNotFound;
