import React from "react";
import { connect } from "react-redux";
import { Segment } from "semantic-ui-react";

import { fetchOrder, clearOrder, confirmOrder } from "redux/actions/order";

const ConfirmOrderPage = () => {
  return (
    <Segment basic>
      <h1>Thank you for your order!</h1>
    </Segment>
  );
};

const mapStateToProps = ({ delivery }) => ({ delivery });

const mapDispatchToProps = { fetchOrder, clearOrder, confirmOrder };

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmOrderPage);
