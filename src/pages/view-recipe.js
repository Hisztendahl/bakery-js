import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { Header, Segment } from "semantic-ui-react";

import { fetchRecipe, clearRecipe } from "redux/actions/recipe";
import { fetchProduct, clearProduct } from "redux/actions/product";
import Markdown from "components/markdown";

const ViewRecipePage = ({
  fetchRecipe,
  clearRecipe,
  recipe,
  fetchProduct,
  clearProduct,
  product,
}) => {
  const { productId } = useParams();
  const error = useRef(false);
  useEffect(() => {
    fetchRecipe(productId);
    fetchProduct(productId);

    return () => {
      clearRecipe();
      clearProduct();
    };
  }, [fetchRecipe, clearRecipe, productId, fetchProduct, clearProduct, error]);

  return (
    <Segment>
      <Header as="h1">{product.name}</Header>
      <Markdown source={recipe.recipe} />
    </Segment>
  );
};

const mapStateToProps = ({ product, recipe }) => ({ product, recipe });

const mapDispatchToProps = {
  fetchRecipe,
  clearRecipe,
  fetchProduct,
  clearProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewRecipePage);
