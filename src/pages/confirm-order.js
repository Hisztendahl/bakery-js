import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Segment, Form } from "semantic-ui-react";
import { useParams, useHistory } from "react-router-dom";

import { fetchOrder, clearOrder, confirmOrder } from "redux/actions/order";

import Trolley from "components/trolley/trolley";

const ConfirmOrderPage = ({
  fetchOrder,
  clearOrder,
  delivery,
  confirmOrder,
}) => {
  const { orderId, confirmationKey } = useParams();
  const history = useHistory();
  useEffect(() => {
    fetchOrder(orderId, confirmationKey);
    return () => clearOrder();
  }, [orderId, confirmationKey, clearOrder, fetchOrder]);
  return (
    <Segment basic>
      <Trolley />
      <ul>
        <li>Type of payment: Cash</li>
        <li>Delivery date: {delivery.renderField("delivery_date")}</li>
      </ul>
      <Form
        onSubmit={async (event) => {
          event.preventDefault();
          try {
            await confirmOrder(orderId, confirmationKey);
            history.push("/order-confirmed");
          } catch (error) {
            alert("Error: " + error.message);
          }
        }}
      >
        <Button type="submit">Confirm Order</Button>
      </Form>
    </Segment>
  );
};

const mapStateToProps = ({ delivery }) => ({ delivery });

const mapDispatchToProps = { fetchOrder, clearOrder, confirmOrder };

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmOrderPage);
