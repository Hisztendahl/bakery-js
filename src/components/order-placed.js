import React from "react";
import { connect } from "react-redux";
import { Header } from "semantic-ui-react";

import StepBar from "components/step-bar";

const OrderPlaced = ({ order }) => {
  return (
    <div>
      <StepBar step="order-placed" />
      <Header as="h3" icon textAlign="center">
        <Header.Content>Thank you for your order!</Header.Content>
      </Header>
      <p>An email has been sent to {order.delivery.email}.</p>
    </div>
  );
};

const mapStateToProps = ({ order }) => ({ order });

export default connect(mapStateToProps)(OrderPlaced);
