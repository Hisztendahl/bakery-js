import React, { useState } from "react";
import { connect } from "react-redux";
import { Button, Form, Message, Segment } from "semantic-ui-react";
import { useHistory } from "react-router-dom";
import SemanticDatepicker from "react-semantic-ui-datepickers";
import moment from "moment";

import StepBar from "components/step-bar";
import "react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css";

import { addDelivery } from "redux/actions/delivery";

const EditDelivery = ({ delivery, addDelivery }) => {
  const history = useHistory();
  const [stateDelivery, setStateDelivery] = useState(delivery);
  const submit = (event) => {
    event.preventDefault();
    addDelivery(stateDelivery, setStateDelivery);
    history.push("/payment");
  };
  return (
    <Segment basic>
      <StepBar step="delivery" />
      <Message info>Food is delivered between 8 and 11 in the morning.</Message>
      <Form onSubmit={submit}>
        <Form.Field>
          <label>The date you wish to be delivered</label>
          <SemanticDatepicker
            onChange={(event, data) =>
              setStateDelivery(
                stateDelivery.handleFieldChange("delivery_date", data.value)
              )
            }
            value={stateDelivery.delivery_date}
            format="DD/MM/YYYY"
            showToday={false}
            filterDate={(date) => {
              const deliveryDate = new Date(); // today
              // deliveries are 2 days from today
              deliveryDate.setDate(deliveryDate.getDate() + 2);
              const deliveryWeekDay = moment(date).local().weekday();
              // Closed on monday [1] and tuesday [2]
              return (
                date > deliveryDate &&
                deliveryWeekDay !== 1 &&
                deliveryWeekDay !== 2
              );
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>First Name</label>
          <input
            onChange={(event) => {
              setStateDelivery(
                stateDelivery.handleFieldChange(
                  event.target.name,
                  event.target.value
                )
              );
            }}
            name="first_name"
            value={stateDelivery.first_name}
            placeholder="First Name"
          />
        </Form.Field>
        <Form.Field>
          <label>Last Name</label>
          <input
            value={stateDelivery.last_name}
            name="last_name"
            onChange={(event) => {
              setStateDelivery(
                stateDelivery.handleFieldChange(
                  event.target.name,
                  event.target.value
                )
              );
            }}
            placeholder="Last Name"
          />
        </Form.Field>
        <Form.Field>
          <label>Phone</label>
          <input
            placeholder="Phone"
            value={stateDelivery.phone}
            name="phone"
            onChange={(event) =>
              setStateDelivery(
                stateDelivery.handleFieldChange(
                  event.target.name,
                  event.target.value
                )
              )
            }
          />
        </Form.Field>
        <Form.Field>
          <label>Email address</label>
          <input
            placeholder="Email address"
            value={stateDelivery.email}
            name="email"
            onChange={(event) => {
              setStateDelivery(
                stateDelivery.handleFieldChange(
                  event.target.name,
                  event.target.value
                )
              );
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>Address Line 1</label>
          <input
            placeholder="Address 1"
            value={stateDelivery.address1}
            name="address1"
            onChange={(event) => {
              setStateDelivery(
                stateDelivery.handleFieldChange(
                  event.target.name,
                  event.target.value
                )
              );
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>Address Line 2</label>
          <input
            placeholder="Address 2"
            value={stateDelivery.address2}
            name="address2"
            onChange={(event) => {
              setStateDelivery(
                stateDelivery.handleFieldChange(
                  event.target.name,
                  event.target.value
                )
              );
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>City</label>
          <input disabled value="Dublin" />
        </Form.Field>
        <Form.Field>
          <label>County</label>
          <input disabled value="Dublin" />
        </Form.Field>
        <Form.Field>
          <label>Leave a message (optional)</label>
          <textarea
            value={stateDelivery.message}
            name="message"
            onChange={(event) => {
              setStateDelivery(
                stateDelivery.handleFieldChange(
                  event.target.name,
                  event.target.value
                )
              );
            }}
          />
        </Form.Field>
        <Button type="submit">Go to Payment</Button>
      </Form>
    </Segment>
  );
};

const mapStateToProps = ({ delivery }) => ({ delivery });
const mapDispatchToProps = { addDelivery };

export default connect(mapStateToProps, mapDispatchToProps)(EditDelivery);
