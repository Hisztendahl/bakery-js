import React from "react";
import { connect } from "react-redux";
import { Grid, Icon, Button, Image } from "semantic-ui-react";

import { deleteItem } from "redux/actions/trolley";

const FirstColumn = ({ product, deleteItem }) => (
  <Grid>
    <Grid.Row>
      <Grid.Column>
        <Image src={product.image} size="tiny" />
      </Grid.Column>
    </Grid.Row>
    <Grid.Row>
      <Grid.Column>
        <Button negative size="mini" onClick={() => deleteItem(product)}>
          Delete <Icon name="close" />
        </Button>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

const mapDispatchToProps = { deleteItem };

export default connect(null, mapDispatchToProps)(FirstColumn);
