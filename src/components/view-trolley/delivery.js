import React from "react";
import { Grid } from "semantic-ui-react";

const Delivery = ({ trolley }) => {
  return (
    trolley.items.length > 0 && (
      <>
        <Grid.Column />
        <Grid.Column textAlign="right">Delivery Price</Grid.Column>
        <Grid.Column textAlign="right">
          &euro;{trolley.delivery_price}
        </Grid.Column>
      </>
    )
  );
};

export default Delivery;
