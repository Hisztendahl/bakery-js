import React from "react";
import { connect } from "react-redux";
import { Grid, Header, Button } from "semantic-ui-react";

import { addOneItem, removeOneItem } from "redux/actions/trolley";

const MiddleColumn = ({ product, trolleyItem, addOneItem, removeOneItem }) => (
  <Grid divided="vertically">
    <Grid.Row columns={2}>
      <Grid.Column>
        <Header as="h3">{product.name}</Header>
      </Grid.Column>
      <Grid.Column>
        <Header as="h3" textAlign="right">
          &euro;{trolleyItem.unit_price}
        </Header>
      </Grid.Column>
    </Grid.Row>

    <Grid.Row columns={3}>
      <Grid.Column textAlign="center">
        <Button size="mini" onClick={() => removeOneItem(product)}>
          -
        </Button>
      </Grid.Column>
      <Grid.Column textAlign="center">{trolleyItem.quantity}</Grid.Column>
      <Grid.Column textAlign="center">
        <Button size="mini" onClick={() => addOneItem(product)}>
          +
        </Button>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

const mapDispatchToProps = {
  addOneItem,
  removeOneItem,
};

export default connect(null, mapDispatchToProps)(MiddleColumn);
