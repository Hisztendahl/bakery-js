import React from "react";
import { Grid } from "semantic-ui-react";

const Total = ({ trolley }) => (
  <>
    <Grid.Column />
    <Grid.Column textAlign="right">Total</Grid.Column>
    <Grid.Column textAlign="right">
      &euro;{trolley.calculateTotal()}
    </Grid.Column>
  </>
);

export default Total;
