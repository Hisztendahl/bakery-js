import React from "react";
import { connect } from "react-redux";
import { Button, Card, Grid, Icon, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { addOneItem } from "redux/actions/trolley";

const ProductCard = ({ product, addOneItem, trolley }) => {
  const trolleyItem = trolley.findTrolleyItem(product);
  return (
    <Card style={{ marginLeft: "auto", marginRight: "auto" }}>
      <Image src={product.image} wrapped ui={false} />
      <Card.Content>
        <Card.Header>
          {product.name}
          <span className="right floated">&euro;{product.price}</span>
        </Card.Header>
        <Card.Description>{product.description}</Card.Description>
      </Card.Content>
      <Card.Content>
        <Grid columns={2}>
          <Grid.Column>
            <Button basic positive fluid onClick={() => addOneItem(product)}>
              <Icon name="shopping cart" />
              Add to trolley
              {trolleyItem && `(${trolleyItem.quantity})`}
            </Button>
          </Grid.Column>
          <Grid.Column>
            <Button
              basic
              color="blue"
              fluid
              as={Link}
              to={`/products/${product.id}/recipe`}
            >
              <Icon name="list" />
              View Recipe
            </Button>
          </Grid.Column>
        </Grid>
      </Card.Content>
    </Card>
  );
};

const mapStateToProps = ({ trolley }) => ({ trolley });

export default connect(mapStateToProps, { addOneItem })(ProductCard);
