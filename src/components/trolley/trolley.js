import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Table } from "semantic-ui-react";

import { fetchProducts, clearProducts } from "redux/actions/product-manager";

const Trolley = ({ trolley, fetchProducts, clearProducts, productManager }) => {
  useEffect(() => {
    fetchProducts();
    return () => clearProducts();
  }, [fetchProducts, clearProducts]);

  return (
    <div>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Product</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Quantity</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Unit Price</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Price</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {trolley.items.map((trolleyItem) => {
            const product = productManager.getProduct(trolleyItem.product_id);
            return (
              <Table.Row>
                <Table.Cell>{product.name}</Table.Cell>
                <Table.Cell textAlign="right">
                  {trolleyItem.quantity}
                </Table.Cell>
                <Table.Cell textAlign="right">&euro;{product.price}</Table.Cell>
                <Table.Cell textAlign="right">
                  &euro;{trolleyItem.calculateTotal()}
                </Table.Cell>
              </Table.Row>
            );
          })}
          {trolley.items.length > 0 && (
            <Table.Row>
              <Table.Cell>Delivery Price</Table.Cell>
              <Table.Cell></Table.Cell>
              <Table.Cell></Table.Cell>
              <Table.Cell textAlign="right">
                &euro;{trolley.delivery_price}
              </Table.Cell>
            </Table.Row>
          )}
          <Table.Row>
            <Table.Cell>Total</Table.Cell>
            <Table.Cell></Table.Cell>
            <Table.Cell></Table.Cell>
            <Table.Cell textAlign="right">
              &euro;{trolley.calculateTotal()}
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    </div>
  );
};

const mapStateToProps = ({ trolley, productManager }) => ({
  trolley,
  productManager,
});

const mapDispatchToProps = { fetchProducts, clearProducts };

export default connect(mapStateToProps, mapDispatchToProps)(Trolley);
