import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Form, Segment } from "semantic-ui-react";
import { useHistory } from "react-router-dom";

import StepBar from "components/step-bar";

import Payment from "domain-objects/payment";
import { placeOrder } from "redux/actions/order";
import { setPayment } from "redux/actions/payment";

const PaymentComponent = ({ placeOrder, setPayment }) => {
  const history = useHistory();
  useEffect(() => {
    setPayment(new Payment({ type: "cash" }));
  }, [setPayment]);
  return (
    <div>
      <StepBar step="payment" />
      <Form
        onSubmit={async (event) => {
          event.preventDefault();
          try {
            await placeOrder();
            history.push("/order-placed");
          } catch (error) {
            alert("Error: " + error.message);
          }
        }}
      >
        <Segment>
          <Form.Field>
            <Form.Radio checked label="Cash" />
          </Form.Field>
        </Segment>
        <Segment>
          <Form.Field disabled>
            <Form.Radio label="Card" />
            Coming soon
          </Form.Field>
        </Segment>
        <Button type="submit">Confirm order</Button>
      </Form>
    </div>
  );
};

const mapDispatchToProps = { placeOrder, setPayment };

export default connect(null, mapDispatchToProps)(PaymentComponent);
