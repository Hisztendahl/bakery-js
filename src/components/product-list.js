import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Grid, Segment } from "semantic-ui-react";
import { Link } from "react-router-dom";

import ProductCard from "components/product-card";
import { fetchProducts, clearProducts } from "redux/actions/product-manager";
import TrolleyButton from "components/trolley/trolley-button";

const ProductList = ({
  trolley,
  message,
  productManager,
  fetchProducts,
  clearProducts,
}) => {
  useEffect(() => {
    fetchProducts();
    return () => clearProducts();
  }, [fetchProducts, clearProducts]);

  return (
    <Segment basic>
      <Grid stackable columns={3}>
        {productManager.items.map((product) => (
          <Grid.Column key={product.name}>
            <ProductCard product={product} />
          </Grid.Column>
        ))}
      </Grid>
      <div style={{ position: "fixed", right: 50, bottom: 20 }}>
        <Button primary size="big" as={Link} to="/view-trolley">
          <TrolleyButton />
        </Button>
      </div>
    </Segment>
  );
};

const mapStateToProps = ({ trolley, message, productManager }) => ({
  trolley,
  message,
  productManager,
});

const mapDispatchToProps = {
  fetchProducts,
  clearProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
