import React from "react";
import { Item } from "semantic-ui-react";

import instagramLogo from "images/instagram.png";

const InstagramLink = () => (
  <div style={{ overflow: "auto", paddingBottom: "1em" }}>
    <Item.Group style={{ float: "right" }}>
      <Item>
        <Item.Content style={{ paddingLeft: ".5em" }} verticalAlign="middle">
          <Item.Image size="mini" src={instagramLogo} />{" "}
          <Item.Header
            as="a"
            href={process.env.REACT_APP_INSTAGRAM_URL}
            target="_blank"
          >
            Check us out on Instagram
          </Item.Header>
        </Item.Content>
      </Item>
    </Item.Group>
  </div>
);

export default InstagramLink;
