import React from "react";
import { connect } from "react-redux";
import { Segment, Step, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

const StepBar = ({ step, trolley, delivery, payment }) => (
  <Segment textAlign="center" basic>
    <Step.Group>
      <Step
        disabled={trolley.isEmpty()}
        active={step === "view-trolley"}
        as={Link}
        to="/view-trolley"
      >
        <Icon name="shopping cart" />
        <Step.Content>
          <Step.Title>View Trolley</Step.Title>
          <Step.Description>View items in trolley</Step.Description>
        </Step.Content>
      </Step>
      <Step
        disabled={
          (trolley.isEmpty() || !delivery.isValid()) && step !== "delivery"
        }
        active={step === "delivery"}
        as={Link}
        to="/delivery/"
      >
        <Icon name="bicycle" />
        <Step.Content>
          <Step.Title>Delivery</Step.Title>
          <Step.Description>Fill out delivery information</Step.Description>
        </Step.Content>
      </Step>
      <Step
        disabled={
          (trolley.isEmpty() || !payment.isValid()) && step !== "payment"
        }
        active={step === "payment"}
        as={Link}
        to="/payment/"
      >
        <Icon name="money" />
        <Step.Content>
          <Step.Title>Payment</Step.Title>
          <Step.Description>Choose payment option</Step.Description>
        </Step.Content>
      </Step>
      <Step disabled={step !== "order-placed"} active={step === "order-placed"}>
        <Icon name="info" />
        <Step.Content>
          <Step.Title>Order Placed</Step.Title>
        </Step.Content>
      </Step>
    </Step.Group>
  </Segment>
);

const mapStateToProps = ({ trolley, delivery, payment }) => ({
  trolley,
  delivery,
  payment,
});

export default connect(mapStateToProps)(StepBar);
