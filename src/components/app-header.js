import React from "react";
import { Header, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";

import logo from "logo.png";

export default () => (
  <Link to="/">
    <Image
      src={logo}
      size="small"
      style={{
        display: "block",
        marginLeft: "auto",
        marginRight: "auto",
      }}
    />
    <Header as="h2" icon textAlign="center" style={{ marginTop: "0.5em" }}>
      <Header.Content>Francois' Bakery</Header.Content>
      <Header sub>Artisan Bread and Pastries</Header>
    </Header>
  </Link>
);
