#!/bin/bash

read -p "Are you sure you want to deploy? (y/n)" -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    now=$(date +'%Y-%m-%d-%H-%M')
    git_branch=$(git rev-parse --abbrev-ref HEAD)
    filename=$now-$git_branch

    scp -r build bakery:bakery-js/$filename
    ssh bakery "cd bakery-js; rm latest; ln -s /home/bakery/bakery-js/$filename /home/bakery/bakery-js/latest"
fi
